import React, { Component } from 'react';
import Block from './Block';
import './Board.css';

class Board extends Component {

    static defaultProps = {
        size: 5
    }

    constructor(props){
        super(props);

        let board = [];
        let topBar = false;
        let bottomBar = false;
        let leftBar = false;
        let rightBar = false;


        for(let x = 0; x < this.props.size ; x++){
            for(let y = 0; y < this.props.size; y++){
                if (x === 0){
                    topBar = true;
                }
                if (x === this.props.size - 1){
                    bottomBar = true;
                }
                if (y === 0){
                    leftBar = true;
                }
                if (y === this.props.size - 1){
                    rightBar = true;
                }

                board.push({
                    active: false,
                    topBar: topBar,
                    bottomBar: bottomBar,
                    leftBar: leftBar,
                    rightBar: rightBar,
                    posX: x,
                    posY: y
                });


                topBar = false;
                bottomBar = false;
                leftBar = false;
                rightBar = false;
            }


        }

        this.state = {
            board: board,
            victory: false
        }

        this.handleClick = this.handleClick.bind(this);
        this.reset = this.reset.bind(this);
        this.win = this.win.bind(this);
    }


    checkVictory(newBoard){

        const inactives = [];
        for(let i = 0; i < newBoard.length; i++){
            if(!newBoard[i].active){
                inactives.push(newBoard[i]);
            }
        }
        
        const inactivesCounter = inactives.length;
        if(inactivesCounter === 0){
            return true;
        } else{
            return false;
        }

    }

    reset(){

        let board = [];
        let topBar = false;
        let bottomBar = false;
        let leftBar = false;
        let rightBar = false;


        for(let x = 0; x < this.props.size ; x++){
            for(let y = 0; y < this.props.size; y++){
                if (x === 0){
                    topBar = true;
                }
                if (x === this.props.size - 1){
                    bottomBar = true;
                }
                if (y === 0){
                    leftBar = true;
                }
                if (y === this.props.size - 1){
                    rightBar = true;
                }

                board.push({
                    active: false,
                    topBar: topBar,
                    bottomBar: bottomBar,
                    leftBar: leftBar,
                    rightBar: rightBar,
                    posX: x,
                    posY: y
                });


                topBar = false;
                bottomBar = false;
                leftBar = false;
                rightBar = false;
            }


        }

        console.log(board);

        this.setState({
            board: board,
            victory: false
        });
        
    }


    win(){

        console.log('WIN');

        const newBoard = [...this.state.board];
        
        for(let i = 0; i < this.state.board.length; i++){
            newBoard[i].active = true;
        }

        this.setState({
            board: newBoard,
            victory: true
        })
    }


    handleClick(evt){

        let newBoard = [...this.state.board];

        newBoard.map((block, index) => {
            if(block.posX === evt.posX && block.posY === evt.posY){
                newBoard[index] = {
                    active: !this.state.board[index].active,
                    topBar: this.state.board[index].topBar,
                    bottomBar: this.state.board[index].bottomBar,
                    leftBar: this.state.board[index].leftBar,
                    rightBar: this.state.board[index].rightBar,
                    posX: this.state.board[index].posX,
                    posY: this.state.board[index].posY
                }

                if(block.topBar === false){
                    newBoard[index - this.props.size] = {
                        active: !this.state.board[index - this.props.size].active,
                        topBar: this.state.board[index - this.props.size].topBar,
                        bottomBar: this.state.board[index - this.props.size].bottomBar,
                        leftBar: this.state.board[index - this.props.size].leftBar,
                        rightBar: this.state.board[index - this.props.size].rightBar,
                        posX: this.state.board[index - this.props.size].posX,
                        posY: this.state.board[index - this.props.size].posY
                    }
                }

                if(block.bottomBar === false){
                    newBoard[index + this.props.size] = {
                        active: !this.state.board[index + this.props.size].active,
                        topBar: this.state.board[index + this.props.size].topBar,
                        bottomBar: this.state.board[index + this.props.size].bottomBar,
                        leftBar: this.state.board[index + this.props.size].leftBar,
                        rightBar: this.state.board[index + this.props.size].rightBar,
                        posX: this.state.board[index + this.props.size].posX,
                        posY: this.state.board[index + this.props.size].posY
                    }
                }

                if(block.leftBar === false){
                    newBoard[index - 1] = {
                        active: !this.state.board[index - 1].active,
                        topBar: this.state.board[index - 1].topBar,
                        bottomBar: this.state.board[index - 1].bottomBar,
                        leftBar: this.state.board[index - 1].leftBar,
                        rightBar: this.state.board[index - 1].rightBar,
                        posX: this.state.board[index - 1].posX,
                        posY: this.state.board[index - 1].posY
                    }
                }

                
                if(block.rightBar === false){
                    newBoard[index + 1] = {
                        active: !this.state.board[index + 1].active,
                        topBar: this.state.board[index + 1].topBar,
                        bottomBar: this.state.board[index + 1].bottomBar,
                        leftBar: this.state.board[index + 1].leftBar,
                        rightBar: this.state.board[index + 1].rightBar,
                        posX: this.state.board[index + 1].posX,
                        posY: this.state.board[index + 1].posY
                    }
                }
            }

            return newBoard;


        })
        
        
        const newVic = this.checkVictory(newBoard);

        this.setState({ board: newBoard, victory: newVic });

    }
    

    render(){

        const board = this.state.board.map(block => {

            return(
                <Block 
                    key={block.posX + ":" + block.posY}
                    value={block}
                    click={this.handleClick}
                    disabled={this.state.victory}
                />
            )
        })

        return(
            <div>
                <div className="Board">
                    {board}
                </div>
                <div className="Board-Bottom">
                    {this.state.victory ? <h2>Victory</h2> : null}
                    <button onClick={this.reset}>Reset</button>
                    <button onClick={this.win}>Win</button>
                </div>
            </div>
        );
    }
}

export default Board;