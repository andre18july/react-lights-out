import React, { Component } from 'react';
import './Block.css';

class Block extends Component {

    constructor(props){
        super(props);

        this.handlerClick = this.handlerClick.bind(this);
    }

    handlerClick(evt){
        this.props.click(this.props.value);
    }


    render(){

        let classBlock = "Block";
        if(this.props.value.active){
            classBlock = classBlock + " BlockActive";
        }

        return(
            <button onClick={this.handlerClick} className={classBlock} disabled={this.props.disabled}></button>
        );
    }
}

export default Block;